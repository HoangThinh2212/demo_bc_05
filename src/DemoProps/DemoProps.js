import React, { Component } from "react";
import UserInfo from "./UserInfo";

export default class DemoProps extends Component {
  renderTitle = () => {
    return <h1>Nice to meet you~</h1>;
  };
  handleClickMe = () => {
    console.log("yesssss");
  };
  render() {
    let user = {
      name: "Bob",
      gmail: "bob@gmail.com",
    };
    return (
      <div>
        <h2>DemoProps</h2>
        {/* Thêm vào hàm khi ng dùng click */}
        <button onClick={this.handleClickMe} className="btn btn-success">
          Click me
        </button>

        {/* tenTuDat và data là tên biến tự đặt ngẫu nhiên và khi truyền thì truyền nội dung sau dấu = của hàm muốn gọi đến nên ko cần () ở tên hàm */}
        <UserInfo tenTuDat={this.renderTitle} data={user} />
      </div>
    );
  }
}
