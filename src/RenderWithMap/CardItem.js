// Component đặt tên sẽ viết hoa cả chữ cái đầu
import React, { Component } from "react";

export default class CardItem extends Component {
  render() {
    return (
      <div className="card text-left h-100">
        {/* nhập vào b4cardalign và chuyển thành file JSX bằng cách bôi đen và chọn nút convert HTML to JSX*/}
        <img
          style={{ height: "80%", objectFit: "cover" }}
          className="card-img-top"
          src={this.props.movie.hinhAnh}
          alt="hình ảnh"
        />
        <div className="card-body">
          <h4 className="card-title text-danger">{this.props.movie.tenPhim}</h4>
          <button className="btn btn-danger">Mua ngay</button>
        </div>
      </div>
    );
  }
}
