import React, { Component } from "react";

export default class DataBinding extends Component {
  // Biến
  imgSrc = "https://movienew.cybersoft.edu.vn/hinhanh/jurassicworld.jpg";
  //   Hàm
  renderDescription = () => {
    return <div>Hello to Alice</div>;
  };
  render() {
    // Data Binding ~ ko cần dùng $ khi muốn import dữ liệu vào
    // Biến bên trong function render()
    let userName = "Alice";
    return (
      <div>
        <div>
          {/* nhập vào b4cardalign và chuyển thành file JSX bằng cách bôi đen và chọn nút convert HTML to JSX*/}
          {/* style là object thì đặt giá trị trong dấu { } ngăn cách bởi dấu , giữa các giá trị và dc binding vào qua dấu { } */}
          <div
            style={{
              width: "250px",
              height: "250px",
              backgroundColor: "aquamarine",
            }}
            className="card text-left"
          >
            {/* Dùng this. để lấy giá trị của imgSrc vì cùng cấp với render() và đặt trong {} ko cần " " */}
            <img className="card-img-top" src={this.imgSrc} alt />
            <div className="card-body">
              <h4 className="card-title">{userName}</h4>
              <p className="card-text">{this.renderDescription()}</p>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
