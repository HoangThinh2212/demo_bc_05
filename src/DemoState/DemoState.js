import React, { Component } from "react";

export default class DemoState extends Component {
  state = {
    like: 0,
  };
  handlePlusLike = () => {
    // this.state.like = 10;
    // setState ~ bất đồng bộ (setState chạy trước console log chạy sau)
    // chỉ để + 1 chứ ko dc để ++ vì nó sẽ update trực tiếp từ số 0 lên số 1
    this.setState({ like: this.state.like + 1 }, () => {
      console.log("thành công", this.state.like);
    });
    // console log này sẽ show giá trị chậm hơn so với giá trị đã hiển thị trên giao diện
    console.log(this.state.like);
  };
  render() {
    return (
      <div>
        <p className="display-4">{this.state.like}</p>
        <button onClick={this.handlePlusLike} className="btn btn-success">
          Plus Like
        </button>
      </div>
    );
  }
}
