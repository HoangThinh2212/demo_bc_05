import React, { Component } from "react";
import { connect } from "react-redux";

class Demo_Mini_Redux extends Component {
  render() {
    return (
      <div>
        <button
          onClick={this.props.handleTangSoLuong}
          className="btn btn-danger"
        >
          Tăng
        </button>
        <span className="display-4 mx-5">{this.props.soLuong}</span>
        <button
          onClick={() => {
            this.props.handleGiamSoLuong(5);
          }}
          className="btn btn-success"
        >
          Giảm
        </button>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    // soLuong là tên bất kì, . là xem number ở numberReducer.js
    soLuong: state.numberReducer.number,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handleTangSoLuong: () => {
      let action = {
        type: "TANG_SO_LUONG",
      };
      dispatch(action);
    },
    handleGiamSoLuong: (value) => {
      dispatch({
        type: "GIAM_SO_LUONG",
        payload: value,
      });
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Demo_Mini_Redux);
