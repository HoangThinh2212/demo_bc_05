import React, { Component } from "react";
import { dataMovie } from "./dataMovie";
import CardItem from "./CardItem";

export default class RenderWithMap extends Component {
  renderMovieList = () => {
    // return về thẻ html
    return dataMovie.map((item, index) => {
      return (
        <div key={index} className="col-3 p-2">
          <CardItem movie={item} />
        </div>
      );
    });
  };
  render() {
    // trong dấu { } gọi là data binding
    return (
      <div className="row container mx-auto">{this.renderMovieList()}</div>
    );
  }
}
