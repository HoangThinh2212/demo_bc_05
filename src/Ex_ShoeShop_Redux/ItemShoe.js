import React, { Component } from "react";
import { connect } from "react-redux";
import { ADD_TO_CART } from "./redux/constant/shoeConstant";
import { CHANGE_DETAIL } from "./redux/constant/shoeConstant";
class ItemShoe extends Component {
  render() {
    let { image, name } = this.props.data;
    // ở đây em lấy props data nè, em truyền vô itemShoe nó mới có
    return (
      <div className="col-3 p-1">
        <div className="card text-left h-100">
          <img className="card-img-top" src={image} alt="" />
          <div className="card-body">
            <h4 className="card-title">{name}</h4>
            <button
              onClick={() => {
                this.props.handleBuyShoe(this.props.data);
              }}
              className="btn btn-success mr-5"
            >
              Buy
            </button>
            <button
              className="btn btn-primary"
              onClick={() => {
                this.props.handleChangeDetail(this.props.data);
              }}
            >
              Detail
            </button>
          </div>
        </div>
      </div>
    );
  }
}
// redux
let mapDispatchToProps = (dispatch) => {
  return {
    handleBuyShoe: (shoe) => {
      let action = {
        type: ADD_TO_CART,
        payload: shoe,
      };
      dispatch(action);
    },
    handleChangeDetail: (shoe) => {
      let action = {
        type: CHANGE_DETAIL,
        payload: shoe,
      };
      dispatch(action);
    },
  };
};

// phải truyền null vào trước thì mới truyền dc hàm mapDispatchToProps
export default connect(null, mapDispatchToProps)(ItemShoe);
