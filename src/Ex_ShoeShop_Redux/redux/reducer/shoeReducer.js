import { ADD_TO_CART, CHANGE_DETAIL } from "../constant/shoeConstant";
import { dataShoe } from "./../../dataShoe";
let initialState = {
  shoeArr: dataShoe,
  detail: dataShoe[0],
  cart: [],
};
export const shoeReducer = (state = initialState, action) => {
  switch (action.type) {
    // Case add to cart
    case ADD_TO_CART: {
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex((item) => {
        return item.id == action.payload.id;
      });
      if (index == -1) {
        //th2: sp chưa có trong giỏ hàng -> tạo mới và push
        let cartItem = { ...action.payload, number: 1 };
        cloneCart.push(cartItem);
      } else {
        //th1: sp đã có trong giỏ hàng -> tăng key number ==> tìm kiếm trong mảng cart biến shoe
        cloneCart[index].number++;
      }
      return { ...state, cart: cloneCart };
    }
    // Case change detail
    case CHANGE_DETAIL: {
      state.detail = action.payload;
      return { ...state };
    }
    default:
      return state;
  }
};
