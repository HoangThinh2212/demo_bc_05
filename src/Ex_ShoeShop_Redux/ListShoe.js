import React, { Component } from "react";
import { connect } from "react-redux";
import ItemShoe from "./ItemShoe";

class ListShoe extends Component {
  renderListShoe = () => {
    // return của list
    return this.props.list.map((item) => {
      // return của map --- là return 1 array
      return (
        <ItemShoe
          handleAddToCart={this.props.handleAddToCart}
          handleViewDetail={this.props.handleChangeDetail}
          data={item}
        />
      );
    });
  };
  render() {
    return <div className="row">{this.renderListShoe()}</div>;
  }
}
let mapStateToProps = (state) => {
  return {
    list: state.shoeReducer.shoeArr,
  };
};
export default connect(mapStateToProps)(ListShoe);
