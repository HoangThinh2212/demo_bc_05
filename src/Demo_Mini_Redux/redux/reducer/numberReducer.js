let initialState = {
  number: 3000,
};
// state và action là default variable của Redux
export const numberReducer = (state = initialState, action) => {
  switch (action.type) {
    case "TANG_SO_LUONG": {
      state.number++;
      return { ...state };
    }
    case "GIAM_SO_LUONG": {
      state.number -= action.payload;
      return { ...state };
    }
    default:
      return state;
  }
};
